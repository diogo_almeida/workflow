import sys 
import psycopg2

param_dict = {
    'host'        : 'localhost',
    'database'    : 'teste',
    'user'        : 'marcelo',
    'password'    : 'teste'
}


def insert_svg(conn, svg, table_name):
    cursor = conn.cursor()
    with open(sys.argv[1]) as svg_file:
        svg = "".join(svg_file.readlines())
    query_text = "INSERT INTO {} (dataset_id, svg_text) VALUES ('{}', '{}')".format(table_name, sys.argv[3] ,svg)
    try:
        cursor.execute(query_text)
        conn.commit()
        print('Table ' + table_name + ' updated successfully')
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()


def connect(param_dict):
    '''
    Connects to the postgres DB
    '''
    conn = None
    try:
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**param_dict)
    except (Exception, psycopg2.DatabaseError) as error:
        print('Connection not successful')
        sys.exit(1)
    print('Connection Successful')
    return conn

