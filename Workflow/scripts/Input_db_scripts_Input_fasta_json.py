import sys 
import psycopg2
import json
import fasta_parser

# adds a dictionary to login on the respective DB ( change values when needed)
param_dict = {
    'host'        : 'localhost',
    'database'    : 'teste',
    'user'        : 'marcelo',
    'password'    : 'teste'
}

def insert_json(conn, data, table_name):
    """
    Using cursor.mogrify() to build the bulk insert query for json data
    then cursor.execute() to execute the query
    """
    # SQL query to execute
    cursor = conn.cursor()
    for k, v in data.items():
        if type(v) is str:
            # SQL query to execute
            query = "UPDATE {} SET {} = '{}'".format(table_name, k, v)
            try:
                # here the data is sent and commited to the DB
                cursor.execute(query)
                conn.commit()
                print('Table ' + table_name + ' updated successfully')
            except (Exception, psycopg2.DatabaseError) as error:
                print("Error: %s" % error)
                conn.rollback()
        else:
            for i, col_data in enumerate(v):
                query = "UPDATE {} SET {} = '{}' WHERE num_id = '{}'".format(table_name, k, col_data, i)
                try:
                    # here the data is sent and commited to the DB
                    cursor.execute(query)
                    conn.commit()
                    print('Table ' + table_name + ' updated successfully')
                except (Exception, psycopg2.DatabaseError) as error:
                    print("Error: %s" % error)
                    conn.rollback()
    cursor.close()


def insert_fasta(conn, fasta_d, table_name):
    """
    Using cursor.mogrify() to build the bulk insert query for fasta data
    then cursor.execute() to execute the query
    """
    # SQL query to execute
    cursor = conn.cursor()
    for i, d in enumerate(fasta_d.items()):
        # SQL querys to execute
        query1 = "INSERT INTO {} (num_id) VALUES ('{}')".format(table_name, i)
        query2 = "UPDATE {} SET seq_name = '{}' WHERE num_id = '{}'".format(table_name, d[0], i)
        query3 = "UPDATE {} SET dna_seq = '{}' WHERE num_id = '{}'".format(table_name, d[1], i)
        query4 = "UPDATE {} SET dataset_id = '{}' WHERE num_id = '{}'".format(table_name, sys.argv[3], i)
        try:
            # here the data is sent and commited to the DB
            cursor.execute(query1)
            conn.commit()
            cursor.execute(query2)
            conn.commit()
            cursor.execute(query3)
            conn.commit()
            cursor.execute(query4)
            conn.commit()
            print('Table ' + table_name + ' updated successfully')
        except (Exception, psycopg2.DatabaseError) as error:
            print("Error: %s" % error)
            conn.rollback()
        
    cursor.close()



def connect(param_dict):
    '''
    Connects to the postgres DB
    '''
    conn = None
    try:
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**param_dict)
    except (Exception, psycopg2.DatabaseError) as error:
        print('Connection not successful')
        sys.exit(1)
    print('Connection Successful')
    return conn



if __name__ == "__main__":
    conn = connect(param_dict)
    with open(sys.argv[1]) as json_data:
        # this is the 1st argument we give the to script and will go to "dados"
        dados = json.load(json_data)
    # this is the 2st argument we give the to script and will go to "data"
    data = open(sys.argv[2])
    fasta = fasta_parser.fasta_dictionary(data)
    insert_fasta(conn, fasta, "datasets")
    insert_json(conn, dados, "datasets" )
    conn.close()
    json_data.close()