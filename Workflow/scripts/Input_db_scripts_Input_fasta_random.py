import sys 
import psycopg2
from fasta_parser import fasta_dictionary

# adds a dictionary to login on the respective DB ( change values when needed)
param_dict = {
    'host'        : 'localhost',
    'database'    : 'teste',
    'user'        : 'marcelo',
    'password'    : 'teste'
}

def insert_fasta(conn, fasta_d, table_name, column_name, dataset_id):
    '''
    This function uses as input the random data 10% a dataset id that the user states to input
    the data on the correct column
    '''
    # SQL query to execute
    cursor = conn.cursor()
    for i, d in enumerate(fasta_d.items()):
        # SQL querys to execute
        query1 = "UPDATE {} SET {} = '{}' WHERE dataset_id = '{}'".format(table_name, column_name, d[1], dataset_id)
        try:
            # here the data is sent and commited to the DB
            cursor.execute(query1)
            conn.commit()
            print('Table ' + table_name + ' updated successfully')
        except (Exception, psycopg2.DatabaseError) as error:
            print("Error: %s" % error)
            conn.rollback()
        
    cursor.close()


def connect(param_dict):
    '''
    Connects to the postgres DB
    '''
    conn = None
    try:
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**param_dict)
    except (Exception, psycopg2.DatabaseError) as error:
        print('Connection not successful')
        sys.exit(1)
    print('Connection Successful')
    return conn





if __name__ == "__main__":
    conn = connect(param_dict)
    data = open(sys.argv[1])
    fasta = fasta_dictionary(data)
    insert_fasta(conn, fasta, "datasets", sys.argv[3], sys.argv[2])
    conn.close()