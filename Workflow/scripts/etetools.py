from ete3 import PhyloTree, TreeStyle
import numpy
import argparse
import os

#### inputs:
#### input_file (-f) = sequências alinhadas em fasta
#### bipartitions_file (-j) = output bipartitions do RAxML
#### newick_file (-k) = output do iqtree do Bruno (tree file)

#### output:
#### visualização das árvores na shell e a criação de imagens no diretório em que se está a trabalhar

##################### Dar como input as variáveis que pretendemos #####################

os.environ["QT_QPA_PLATFORM"] = "offscreen"


PARSER = argparse.ArgumentParser()

PARSER.add_argument("-f", type=argparse.FileType(), dest="input_file", help="select input file")

PARSER.add_argument("-j", type=argparse.FileType(), dest="bipartitions_file", help="select bipartitions file")

PARSER.add_argument("-k", type=argparse.FileType(), dest="newick_file", help="select newick file")

PARSER.add_argument("-b", type=str, dest="newick_svg_file", help="select newick svg name")

PARSER.add_argument("-m", type=str, dest="raxml_svg_file", help="select raxml file")

ARGUMENTS = PARSER.parse_args()

##################### Construir as árvores filogenéticas ##########################

fasta_txt = "".join(ARGUMENTS.input_file.readlines())

tree_newick = PhyloTree("".join(ARGUMENTS.newick_file.readlines()), quoted_node_names=False, format=1)

print("Phylogenetic Tree Newick", tree_newick)

tree_raxml = PhyloTree("".join(ARGUMENTS.bipartitions_file.readlines()),alignment=fasta_txt, alg_format="fasta")

print ("Phylogenetic Tree RAxML", tree_raxml)

################ Visualizar as imagens das árvores #####################

raxml_txt = "".join(ARGUMENTS.bipartitions_file.readlines())

newick_txt = "".join(ARGUMENTS.newick_file.readlines())


################ Ficheiros SVG #####################

BI_svg = ARGUMENTS.newick_svg_file

ML_svg = ARGUMENTS.raxml_svg_file


tn = tree_newick
tr = tree_raxml

ts = TreeStyle()
ts.show_leaf_name = False
ts.show_branch_length = False
ts.show_branch_support = True
ts.scale =  300
ts.branch_vertical_margin = 10

tn.render(BI_svg, w=750, units="mm", tree_style=ts)
tr.render(ML_svg, w=750, units="mm", tree_style=ts)
