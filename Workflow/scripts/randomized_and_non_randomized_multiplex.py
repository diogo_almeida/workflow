#!/user/bin/python3

'''
Script to repeat randomized and non-randomized n times.
'''

import sys
import class_arguments
import randomized

if __name__ == "__main__":
    ARGS = class_arguments.ArgumentParser(sys.argv[1:])
    ARGS.PARSER.add_argument("-n", dest="number_repeat", type=int, help="Number of times to repeat"\
, required=True)
    FASTA_HANDLE = ARGS.argument_handler().input_file
    PERCENTAGE = ARGS.argument_handler().desired_percentage
    SEED = ARGS.argument_handler().random_seed
    randomized.seed(SEED)
    SEQUENCES = randomized.fasta_parser.fasta_dictionary(FASTA_HANDLE)
    ULTIMATE_DICTIONARY = randomized.inserting_missing_data(SEQUENCES, PERCENTAGE)

#O seed tem que ser != para cada iteração redirecionar para n ficheiros !=
    for i in range(ARGS.argument_handler().number_repeat):
        randomized.fasta_writer(ULTIMATE_DICTIONARY)
